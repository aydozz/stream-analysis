Compiles, executes and plots stream benchmark results for different array lengths

The goal is to expose the memory hierarchy

Single threaded so far (adding -fopenmp to compilation creates unexpected results with very low performance on short arrays)

usage:

> make stream_all

> perl plot.pl

this will execute the benchmarks and generate two files

1) gnuplot.script

2) stream_bw.dat

perl plot.pl uses stream_bw.dat as its input. To generate the actual plot:

> gnuplot gnuplot.script

This will generate the figure stream_bw.eps

Results:
1- On my Mac (Processor: https://ark.intel.com/products/75991/Intel-Core-i5-4288U-Processor-3M-Cache-up-to-3_10-GHz, Memory: 1600MHz DDR3L)
![stream_bw_i5-4288U_Mac.png](https://bitbucket.org/repo/g8pkxd/images/2244812656-stream_bw_i5-4288U_Mac.png)

2- On Cori Haswell partition (Processor: http://ark.intel.com/products/81060/Intel-Xeon-Processor-E5-2698-v3-40M-Cache-2_30-GHz, Memory: DDR4 2133 MHz)
![stream_bw_cori_haswell.png](https://bitbucket.org/repo/g8pkxd/images/1551753376-stream_bw_cori_haswell.png)
CC = cc
CFLAGS = -O2

stream_all: stream.c
	for number in 8000 64000 256000 1024000 4096000 16384000; do \
		echo "$(CC) $(CFLAGS) -DSTREAM_ARRAY_SIZE=$$number -o stream_$$number stream.c"; \
		$(CC) $(CFLAGS) -DSTREAM_ARRAY_SIZE=$$number -o stream_$$number stream.c; \
	done;

clean:
	rm -f stream_* *.o


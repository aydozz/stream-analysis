#!/usr/bin/perl -w
# Executes and plots stream benchmark results for different array length
# Tries to expose the memory hiearchy
# Initial version: 1/16/17 by Aydin Buluc
# Single threaded so far (adding -fopenmp to compilation creates unexpected results)

my $gnuplot = "set terminal postscript color 18\
set logscale x\
set xlabel \"Array size\"\
set ylabel \"Bandwidth (MB/s)\"\
set style line 1 lt 1 lw 4 \
set output \"stream_bw.eps\"\
plot 'stream_bw.dat' using 1:2 title 'Stream Triad Bandwidth' w linespoints ls 1\n";

my @bandwidth;

open(my $fdat, '>', 'stream_bw.dat') or die "Could not open file 'stream_bw.dat' $!";
print $fdat "ArrayLength \t Bandwidth\n";
my @lengths = (8000, 64000, 256000, 1024000, 4096000, 16384000);
for (my $i = 0; $i < @lengths; $i++) {
	my $cmd = "./stream_" . $lengths[$i];
	print $cmd . "\n";
	my $output = `$cmd`;

	if ($?) {
		die "Error running [$cmd]";
	}
	else {
		open my $olines, '<', \$output or die $!;
 		while (<$olines>) {
   			if($_ =~ /^Triad/) {
				print $_;
                my @words = split /\s+/, $_;
                push @bandwidth, $words[1];
                my $datline = $lengths[$i] . " \t " . $bandwidth[$i] . " \n";
                print $fdat $datline;
			}
   		}
 		close $olines or die $!;
	}
}

open(my $fgnu, '>', 'gnuplot.script') or die "Could not open file 'gnuplot.script' $!";
print $fgnu $gnuplot;
close $fdat;
